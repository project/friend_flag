## INTRODUCTION

Friendship module.

Very simple "friend" module built upon Flag module.  After you install you'll 
have 'friend' flag and a block you can place via your block layout area. The 
block is a table with two columns: 
Friend Requests (with a link to add the user as a friend), and a Friends 
column. The basic principle is a user clicks the flag on another user, that 
is a friend request. If two users each click the  flag on the other's profile 
page, they are friends.

## REQUIREMENTS

This module will requires the flag module version 8.x-4.0-beta2 for Drupal 9.

## INSTALLATION
 
Install as you would normally install a contributed Drupal module. Visit:
[https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules]
for further information.  It is the same process for Drupal 9.

## CONFIGURATION
After installing, you'll see a 'friend' flag in your flags area 
/admin/structure/flags.  Edit the flag to change the text etc.  You'll also 
have a block available to place via your Block layout area 
/admin/structure/block.  Place the block to an area and/or limit to certain 
pages as you would any block.  Each users who then sees the page with block will
be able to see their friends and friend requests.
