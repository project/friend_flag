<?php

namespace Drupal\friend_flag\Plugin\Block;

use Drupal\user\Entity\User;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\flag\FlagService;
use Drupal\flag\FlagLinkBuilder;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates a block to show the users friend activity.
 *
 * We'll create four arrays in total, friend requests, flag links for friend
 * requests, friends, and flag links for friends.  The createFriendRequestsArray
 * creates friend requests and accompanying links, and the
 * createFriendsArray does the same for friends.
 */

/**
 * Block declaration.
 *
 * @Block(
 *   id = "friend__flag_block",
 *   admin_label = @Translation("Friend")
 * )
 */
class FriendFlagBlock extends Blockbase implements ContainerFactoryPluginInterface {
  /**
   * @var AccountInterface $account
   */
  protected $account;
  private $flag;
  private $flag_link_builder;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $account, FlagService $flag, FlagLinkBuilder $flag_link_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->account = $account;
	$this->flag_service = $flag;
	$this->flag_link_builder = $flag_link_builder;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $flag = $container->get('flag');
	$flag_link_builder = $container->get('flag.link_builder');
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
	  $flag,
	  $flag_link_builder
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    return [
      '#theme' => 'friend_flag_template',
      '#friend_requests' => $this->createFriendRequestsArray()[0],
      '#friends_requests_links' => $this->createFriendRequestsArray()[1],
      '#friends' => $this->createFriendsArray()[0],
      '#friends_links' => $this->createFriendsArray()[1],
      '#attached' => [
        'library' => [
          'friend_flag/friend_flag',
        ],
      ],
    ];
  }

  /**
   * Creates the Friend Requests column and the corresponding flag links.
   */
  private function createFriendRequestsArray() {
	$current_user = $this->account->id();
    $userMe = User::load($current_user);
    $flag_service = $this->flag_service;
	$flag_link = $this->flag_link_builder;
    $friendFlag = $flag_service->getFlagById('friend');
    $friends_requests_links = [];

    // getFlaggingUsers from flag module will return users who have flagged the
    // specified entity, in our case the current user '$userMe'.
    $friendRequests = $flag_service->getFlaggingUsers($userMe, $friendFlag);

    // We don't want to include in the requests list people who are already
    // friends, so need to remove them from array.  getEntityFlaggings
    // will check to see if there's a flag that exists where current user has
    // flagged each user in the array.  If so, removes it.
    foreach ($friendRequests as $friendRequestKey => $friendRequest) {
      if (!empty($flag_service->getEntityFlaggings($friendFlag, $friendRequest, $userMe))) {
        unset($friendRequests[$friendRequestKey]);
      }
      else {
        $friends_requests_links[] = $flag_link->build('user', $friendRequest->id(), 'friend');
      }
    }
    return [$friendRequests, $friends_requests_links];
  }

  /**
   * Creates the Friends Requests column and the corresponding flag links.
   *
   * Almost identical process to createFriendRequestsArray, but now instead
   * of removing users from the array created by createFriendRequestsArray
   * when each user has flagged each other, we're creating a new array of
   * Friends.
   */
  private function createFriendsArray() {
	$current_user = $this->account->id();
    $userMe = User::load($current_user);
    $flag_service = $this->flag_service;
	$flag_link = $this->flag_link_builder;
    $friendFlag = $flag_service->getFlagById('friend');
    $friends = [];
    $friends_links = [];
    $friendRequests = $flag_service->getFlaggingUsers($userMe, $friendFlag);

    foreach ($friendRequests as $friendRequest) {
      if (!empty($flag_service->getEntityFlaggings($friendFlag, $friendRequest, $userMe))) {
        $friends[] = $friendRequest;
        $friends_links[] = $flag_link->build('user', $friendRequest->id(), 'friend');
      }
    }
    return [$friends, $friends_links];
  }

}
